from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()



class Department(Base):
     __tablename__ = 'department'
     id = Column(Integer, primary_key=True)
     name = Column(String)
     employees = relationship('Employee', secondary='department_employee')


class Employee(Base):
     __tablename__ = 'employee'
     id = Column(Integer, primary_key=True)
     name = Column(String)
     departments = relationship('Department', secondary='department_employee')


class DepartmentEmployeeLink(Base):
     __tablename__ = 'department_employee'
     department_id = Column(Integer, ForeignKey('department.id'), primary_key=True)
     employee_id = Column(Integer, ForeignKey('employee.id'), primary_key=True)

from sqlalchemy import create_engine
engine = create_engine('sqlite:///tester.db')

class DataAccessLayer(object):
    connection = None
    engine = None
    conn_string = None
    metadata = MetaData()
    departments = Department()
    employees = Employee()
    departmentemployeelinks = DepartmentEmployeeLink()

    def db_init(self, conn_string): 2
        self.engine = create_engine(conn_string or self.conn_string)
        self.metadata.create_all(self.engine)
        self.connection = self.engine.connect()

dal = DataAccessLayer()

john = dal.Employee(name='john')
s.add(john)
it_department = Department(name='IT')
it_department.employees.append(john)
s.add(it_department)
s.commit()


john = s.query(Employee).filter(Employee.name == 'john').one()
john.departments

print(john.departments[0].name)

it = s.query(Department).filter(Department.name == 'IT').one()
it.employees
print(it.employees[0].name)


marry = Employee(name='marry')
financial_department = Department(name='financial')
financial_department.employees.append(marry)
s.add(marry)
s.add(financial_department)
s.commit()
