
#%% Delete Function (For starting again...)
#def delete_db()

#%% SQLALCHEMY IMPORTS
from sqlalchemy import create_engine, select
from sqlalchemy.orm import sessionmaker
from datetime import datetime
from sqlalchemy import (Table, Column, Integer, Numeric, String, DateTime,
ForeignKey, Boolean)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

#%% START ENGINE AND SESSION
engine = create_engine('sqlite:///:memory:')
Session = sessionmaker(bind=engine)
session = Session()


#%% DEFINE TABLES

Base = declarative_base()

class Contact(Base):
	__tablename__ = 'contacts'                                      #
	id = Column(Integer, primary_key=True)                   #
	name = Column(String(50), index=True)                    #

	def __init__(self, name):                                            #
 		self.sample_name = name

	def __repr__(self):
		return "Contact(name='{self.name}', ".format(self=self)


class Nickname(Base):
	__tablename__ = 'nicknames'                                       #
	id = Column(Integer, primary_key=True)                   #
	nickname = Column(String(50), index=True)                    #                          #

	def __init__(self, name):
		self.name = name

	def __repr__(self):
		return "Program(program_name='{self.program_name}')".format(self=self)


#%% CREATE ALL
Base.metadata.create_all(engine)

import unittest
#from models import *

class TestMain(unittest.TestCase):

    def test_just_name(self):
        '''
        Insert a contact and get it with just his name
        if there are no others nicknames.
        '''
        # Decide what you want
        expected = {'name' : 'Steve'}
        # Init
        contact = Contact(name='Steve') 
        session.add(contact) # s.query(Contact).first()
        session.commit()
        actual = session.query(Contact).first()
        print(dir(actual))
        print(actual)
        self.assertEqual(expected, actual)
        
if __name__ == '__main__':
    unittest.main()
