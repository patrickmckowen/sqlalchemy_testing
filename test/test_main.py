import unittest
from models2 import *

class TestMain(unittest.TestCase):
    def setUp(self):
        self.engine = create_engine('sqlite:///:memory:')
        self.session = sessionmaker()
        self.session.configure(bind=self.engine)
        Base.metadata.create_all(self.engine)
        self.session = self.session()
        
        self.contact = Contact(name="Steve")
        self.nickname1 = Nickname(name="Steve-O")
        self.nickname2 = Nickname(name="Stevey")
        
        self.contact.nicknames.append(self.nickname1)
        
        self.nickname2.contacts.append(self.contact)
        self.session.add(self.contact)
        self.session.add(self.nickname1)
        self.session.add(self.nickname2)
        self.session.commit()

    def tearDown(self):
        Base.metadata.drop_all(self.engine)

    def test_get_contact_name(self):
        expected = 'Steve'
        actual = self.session.query(Contact)\
                 .filter(Contact.name == 'Steve')\
                 .first().name
        #print(dir(actual))
        self.assertEqual(actual, expected)



    def test_get_contact_name_and_nicknames(self):
        expected = 'Steve'
        actual = self.session.query(Contact).filter(Contact.name == 'Steve').first()
        #actual = self.session.query(Contact).filter(Contact.name == 'Steve')
        actual =  self.session.query(Nickname).contains(actual.name).all()
        self.assertEqual(actual, expected)
"""
# Find all Address whose person field is pointing to the person object
>>> 

    def test_just_name(self):
        '''
        Insert a contact and get it with just his name
        if there are no others nicknames.
        '''
        # Decide what you want
        expected = 'Steve'
        # Init
        contact = 
        s.add(contact)
        s.commit()
        
        self.assertEqual(expected, actual)
        """
"""
    def test_just_name(self):
        '''
        Insert a contact and get it with just his name
        if there are others nicknames.
        '''
        # Decide what you want
        expected = {'name' : 'Steve', 'nicknames' : []
        # Init
        contact = Contact(username='Steve') 
        session.add(contact)
        session.commit()
        actual = user.add()
        #self.assertEqual('foo'.upper(), 'FOO')

        
    def test_name_empty_nicknames(self):
        expected = {'name' : 'Steve', 'nicknames' : []}
        #self.assertEqual('foo'.upper(), 'FOO')

    def test_name_empty_nicknames(self):
        User(username='admin', email='admin@example.com')
        expected = {'name' : 'Steve', 'nicknames' : ['Stevo', 'Stevey']}
        #self.assertEqual('foo'.upper(), 'FOO')
        
    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)
"""

if __name__ == '__main__':
    unittest.main()
